=================
How to Contribute
=================

Feel free to contribute, fork, use, etc.

:github: https://github.com/gpalsingh/Simple-Menus
:gitlab: https://gitlab.com/gurkirpal204/Simple-Menus

Although I do work on github but have also created a copy of the
project on gitlab.
