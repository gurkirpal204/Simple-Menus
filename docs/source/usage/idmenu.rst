====================
IdentifierMenu Class
====================

This class is used to create menus which have and identifier
for each option. You will most likely use this class for almost
all of your purposes. It allows all the arguments that `Menu`_
class has plus some of it's own.

join
====
This argument specifies whether to join the identifier and the option
or not. By default it is false and only spaces will be present
in place of joiner. If this is set to ``True`` and no ``joiner`` is
given then default joiner (``"."``) will be used.

joiner
======

This argument is used to specify which joiner to use instead of a
space. It is needed to be a single character because we internally
use the ``str.format`` function.

**Note:** The ``join`` argument is only for use when you don't want
to give your own joining character. It would be silly to give set
both ``join`` and ``joiner``.

idfunc
======

This is the function that will be called on the identifiers. It
should accept integer and output a string. The output should be
distinct for distinct input. If that is not the case then very
bad things could happen as two options would have the same
id. 

style
=====

Curently there are only two styles namely ``"number"`` and ``"alphabet"``.
The ``"number"`` argument just shows plain numbers along with options.
Note that currently you cannot call function on ids if you set the style
as ``"alphabet"`` as it itself is done by setting the ``idfunc`` variable.

\*args and \*\*kwargs
=====================

Both of these are ignored.


.. _`Menu`: menu.html
