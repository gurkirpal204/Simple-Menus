==========
Menu class
==========

This class acts as the base class and provides most of the
arguments for configuring the menus. Here the arguments are
explained which this class accepts.  If you are looking for
how this class works then check out the `guide`_.

options
=======

This is the most important argument that you need to set. The
passed in value must be subscriptable collection of items.
If there are repeating items then they will automatically
be removed.

ambiguous
=========

This is the most useful feature of this class. Setting it to true
allows the user to select multiple options at once, without throwing
any error. Whether to return the results or further ask the user to
narrow down the selection can also be configured. Here is an example
that allows multiple selections::

    menu = IdentifierMenu(options=['first', 'second'],
                                   'prompt='Enter your choice : ')


resolve
=======

Usually you would want the user to allow both selecting many options
at a time and also return only one option at last. If this is set as
``True`` then the function will not return until the user has selected
a single option. By default it is true. It should only be used if you
want advanced control over multiple selections.

columns
=======

This argument allows you to set the number of columns to split the
options into. The number can by any integer. For example: To split the
options into three columns::

    menu = IdentifierMenu(options=[str(x)*4 for x in range(30)],
                          columns=3)

rows
====

This is very similar to the ``columns`` argument. 

**Note:** If you set both ``rows`` and ``columns`` then only the effects
of ``rows`` will be shown. If the number in any of the cases is not a divsor
of number of options then the resulting table not look like what you expect.

menu_style
==========

This option is only useful if you change the ``rows`` or ``columns``. There
are only two possible values. If you set it to ``"horizontal"`` then the options
will run horizontally, but if you set it as ``"vertical"`` then they will be
shown in vertical direction (like in a dictionary).

sort
====

This options tells if the program should sort the options before showing them.
It is only useful if there are large number of options. It can be set to
`True` of `False`. Internally we just use the ``sort`` function provided by
the standard library.

reverse
=======

It specifies if the options should be sorted in reverese order or not. This could
be helpful has user would look at the lower options first. It accepts boolean.

key
===

This option is for giving a custom function as key to sort the options.

func
====

This argument specifies a function that must be applied on the items before showing
them in the menu. Note that the returned item will be the original one and not what
the user sees. This could be used to capitalize the options::

    menu = IdentifierMenu(options='small words everywhere'.split(),
                          func=str.capitalize)

The function is required to return string although it could accept any type you want.

help_text
=========

By default the ``help_text`` is empty and no help text is shown. You can set any text
you want by setting this argument.

help_pos
========

This argument tells where to put the ``help_text``. By default it is put at the bottom
by using ``"bottom"``. You can put at the top by setting it to ``"top"`` which is the
only other acceptable value.

is_main_menu
============

You should really not change this variable except only in when you are handling multiple
options manually. Setting ``is_main_menu=True`` tells the menu that it is a sub_menu. The
menu would add an additional help text to the previous one and would return if the value
entered by user maps to no option instead of running forever.

lazy
====

This options is used to delay construction of menu until it is needed. This might be needed
if construction of menu takes significant amount of time and it is not certain whether
it would be needed or not. Setting ``lazy=True`` would delay it's creation. It is ``False``
by default.

\*args and \*\*kwargs
=====================

These options are just for allowing easier creation of objects and are ignored.


.. _`guide`: ../making.html
