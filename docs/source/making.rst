=======================
Making your own classes
=======================

The classes in this library inherit from the ``Menu``
class which is the main class. The structure of this class is
explained here which will allow you to make your own custom
menu classes.

Overall structure
=================

The structure of the class is as shown

.. image:: assets/menu.png
    :alt: Overall structure

Note that the above is **not** a flow diagram. Instead it
just simply tells what function calls which function and which
functions are needed to be overriden, etc

    :Blue methods: These methods are pre-defined and are not meant
        to be changed in any case.
    :Yellow methods: These methods usually don't do much and could
        be overridden at user's will.
    :Green methods: Theses methods are required to be overridden
        by the child classes.

The arrows tell which function calls which one. This means that
``get_response`` calls ``get_matching_items``. The two types of
arrows have different meanings:

    :Black arrow: The call must be made.
    :Red arrow: Optional to call.

Function Roles
==============

Different functions serve different roles which are explained
below.

__init__
--------

This is used to set the initial values of the object. It also
class ``make_menu`` if ``lazy=False`` is given.

make_menu
---------

This is the most important function that should be defined by
the child. It takes care of setting the important variables
which are explained below.

    :pic_blocks: This variable contains a list of the strings
        of rows that would be used by ``make_picture``.
    :picture: This variable contains the string that will be
        shown to the user. It should be set if ``make_picture``
        is not used.
    :menu_made: This variable should be set to ``True`` to avoid
        unnecessary recreation of menu.

This function also needs to set some variables for ``get_matching_items``
which are implementation specific.

get_matching_items
------------------

This function accepts a string and returns the option(s) that match
the given criteria. It should check ``self.ambiguous`` variable and
work accordingly.

show_menu
---------

This function simply checks where a menu is already made or not and
prints ``self.picture``.

get_real_items
--------------

This function is necessary to set if you want to use the ``func``
option and don't want the program to behave strangely. This function
should accept an iterable containg the items with ``func`` applied
on them and it should return an iterable containing items converted
to the format which you want to get them when they are returned/reused.
You would most likely not want to use it but it would be helpful
if you run into some problems.

get_valid_response
------------------

This function accepts input from user and checks whether it is acceptable.
In case it is acceptable, it is returned otherwise the process is repeated.
The default function does no checking and returns the string no matter
what it is.

Examples
========

An example class that uses these is inherits form ``Menu`` is
`IdentifierMenu`_.

.. _`IdentifierMenu`: https://github.com/gpalsingh/Simple-Menus/blob/master/simplemenus/smenus.py#L245
