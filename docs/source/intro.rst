============
Introduction
============

Here are some examples which will help you to get
started with ``simplemenus``.

Creating a basic menu
=====================

All you need to write a simple menu is just three lines
of code::

    from simplemenus import IdentifierMenu

    menu = IdentifierMenu(options=['foo', 'bar'])
    menu.get_response()

The class will do all the necessary work necessary and give
you your nice menu. The options could be selected either by
giving the number or the starting substring. This means you
can select ``foo`` either by entering ``1`` or ``f`` or ``fo``
but not ``o`` or ``oo``.

Changing the prompt
===================

The default character shown to the user is ``>``. To change it
you just need to set the ``prompt`` argument::

    menu = IdentifierMenu(options=['foo', 'bar'],
                          prompt='Enter your choice : ')

Sort options before showing menu
================================

Sometimes it might be desirable to sort the options before
displaying them. The ``sort`` argument is for that purpose::

    menu = IdentifierMenu(options=['foo', 'bar'],
                          sort=True)

For more details check out the `IdentifierMenu`_ and `Menu`_ docs.


.. _`IdentifierMenu`: usage/idmenu.html
.. _`Menu`: usage/menu.html
