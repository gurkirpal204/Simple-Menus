# Simple Menus

*Simple menus for humans*

[![PyPI version](https://badge.fury.io/py/Simple-Menus.svg)](https://badge.fury.io/py/Simple-Menus)
[![Documentation Status](https://readthedocs.org/projects/simple-menus/badge/?version=latest)](http://simple-menus.readthedocs.org/en/latest/?badge=latest)
[![Codacy Badge](https://api.codacy.com/project/badge/grade/c5ed04b434004d06b37991e9a0304c89)](https://www.codacy.com/app/gurkirpal204/Simple-Menus)
[![Code Climate](https://codeclimate.com/github/gpalsingh/Simple-Menus/badges/gpa.svg)](https://codeclimate.com/github/gpalsingh/Simple-Menus)
[![Code Health](https://landscape.io/github/gpalsingh/Simple-Menus/master/landscape.svg?style=flat)](https://landscape.io/github/gpalsingh/Simple-Menus/master)
[![Issue Count](https://codeclimate.com/github/gpalsingh/Simple-Menus/badges/issue_count.svg)](https://codeclimate.com/github/gpalsingh/Simple-Menus)
[![Test Coverage](https://codeclimate.com/github/gpalsingh/Simple-Menus/badges/coverage.svg)](https://codeclimate.com/github/gpalsingh/Simple-Menus/coverage)
[![MIT License](https://img.shields.io/badge/license-MIT-blue.svg?style=flat)](https://github.com/gpalsingh/Simple-Menus/blob/master/LICENSE)

Why?
----

Usually one has to make the user choose an item from a list of items. For this
one has to write the same code again. The result is usually a very ugly menu
which is not much user friendly. Simple Menus is a simple library that provides ready made
classes to make menus which are smart and at the same time good looking and user friendly.
You say that you can do that with a while loop you say? These menus are error
tolerant which means that if user enters an input that maps to more than one option,
it automatically picks those options and shows a new menu with only those options
to the user. 

![Example1](assets/game.gif?raw=true)

[code](https://github.com/gpalsingh/Simple-Menus/blob/master/examples/game.py)

This feature can be turned on and off while making a new menu.

Usage
-----

To make a menu with options ``UP`` and ``Down`` use the following

```python
from simplemenus import IdentifierMenu

menu = IdentifierMenu(options=['Up', 'Down'])
res = menu.get_response()
```

After the user makes the choice the choice will be stored in ``res``.

Right now this library provides only the ``IdentifierMenu`` class to make menus. This
class accepts many different arguments which can be used to make a wide variety
of menus. If you are still not satisfied and want more, you can make your own menu classes by
extending the abstract base class ``Menu`` which acts the base class for all menus in this
library. 

To know more about how to use the library head over to [the docs](http://simple-menus.readthedocs.org).
