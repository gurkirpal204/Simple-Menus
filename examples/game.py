from colorama import Fore, Style, init
from simplemenus import IdentifierMenu

init(autoreset=True)

def green(i):
    return Fore.GREEN + Style.BRIGHT + str(i) + Style.RESET_ALL
def yellow(st):
    return Fore.YELLOW + Style.BRIGHT + st.capitalize() + Style.RESET_ALL

m = IdentifierMenu(options='north south east west'.split(),
                   ambiguous=True,
                   idfunc=green,
                   func=yellow,
                   help_pos='top',
                   help_text='''
     _____________________
    |                     |
    | WHERE SHOULD WE GO? |
    |_____________________|
    |
    |   O   \o/
    |  /|\   |
 ------------------
 \                /
  \______________/.`'~~.

''',
                   joiner='.',
                   columns=2,
                   )
print "Let's move towards {} !".format(m.get_response())
